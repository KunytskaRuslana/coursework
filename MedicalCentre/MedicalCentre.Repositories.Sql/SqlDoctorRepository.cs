﻿using MedicalCentre.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalCentre.Repositories.Sql
{
    public class SqlDoctorRepository : IDoctorRepository
    {
        private string connectionString;

        static string querySelectDoctors = "SELECT [Name],[Description]"
                                    + "FROM [dbo].[Doctor]"
                                    + "WHERE [Description] != ' '";

        public SqlDoctorRepository(string _connectionString)
        {
            this.connectionString = _connectionString;
        }

        public List<DoctorEntity> SelectAllDoctors()
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = this.connectionString;
                connection.Open();
                using (SqlCommand command = new SqlCommand(querySelectDoctors, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        List<DoctorEntity> doctorEntity = new List<DoctorEntity>();
                        while (reader.Read())
                        {
                            doctorEntity.Add(new DoctorEntity()
                            {
                                Name = (string)reader["name"],
                                Description = (string)reader["description"]
                            });
                        }
                        return doctorEntity;
                    }
                }
            }
        }
    }
}
