﻿using MedicalCentre.Repositories;
using MedicalCentre.Repositories.Sql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MedicalCentre.WebUI.Controllers
{
    public class MedicalCentreController : Controller
    {
        private readonly IDoctorRepository _doctorRepository;

        public MedicalCentreController(IDoctorRepository doctorRepository)
        {
            this._doctorRepository = doctorRepository;
        }

        //
        // GET: /MedicalCentre/
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Doctors = _doctorRepository.SelectAllDoctors();
            return View();
        }

        public ActionResult Phone()
        {
            return View();
        }

        public ActionResult Workday()
        {
            return View();
        }

        public ActionResult Appointments()
        {
            return View();
        }

    }
}
