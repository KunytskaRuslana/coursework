﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalCentre.Entities
{
    public class AppointmentEntity
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public int DoctorId { get; set; }
        public string Complaint { get; set; }
        public int Order { get; set; }
        public DateTime Date { get; set; }
    }
}
