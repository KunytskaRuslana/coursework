﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalCentre.Entities
{
    public class DoctorEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime HourFrom { get; set; }
        public DateTime HourTo { get; set; }
        public string Description { get; set; }
    }
}
