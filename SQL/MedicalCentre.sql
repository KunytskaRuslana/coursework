create database MedicalCentre;
go
use MedicalCentre;
go
-- create table
create table Doctor
(
	Id int not null identity(1,1),
	[Name] nvarchar(25) not null,
	HourFrom Time not null,
	HourTo Time not null,
	[Description] nvarchar(1024) not null
	constraint PK_Doctor_Id primary key (Id)
)
go
create table NameDay
(
	Id int not null identity(1,1),
	NameD nvarchar(25) not null
	constraint PK_NameDay_Id primary key (Id)
)
go
create table WorkDay
(
	Id int not null identity(1,1),
	DoctorId int not null,
	DayId int not null
	constraint PK_WorkDay_Id primary key (Id)
)
go
create table Patient
(
	Id int not null identity(1,1),
	Surname nvarchar(30) not null,
	[Name] nvarchar(30) not null,
	Phone nvarchar(15) not null
	constraint PK_Patient_Id primary key (Id)
)
go
create table Appointment
(
	Id int not null identity(1,1),
	PatientId int null,
	DoctorId int not null,
	Complaint nvarchar(120) null,
	[Order] int not null,
	[Date] Datetime not null
	constraint PK_Appointment_Id primary key (Id)
)
go
create table UserRecord
(
	Id int not null identity(1,1),
	UserName nvarchar(50) not null,
	[Password] nvarchar(50) not null,
	PatientId int not null
	constraint PK_UserRecord_Id primary key (Id, PatientId)
)
go
-- insert data
set identity_insert Doctor on
insert into Doctor 
		(Id, [Name], HourFrom, HourTo, [Description]) 
	values 
		(1, '��������', '08:00:00', '17:00:00', ''),
		(2, '������������', '08:00:00', '17:00:00', ''),
		(3, '�����������', '08:00:00', '17:00:00', ''),
		(4, '�����������', '08:00:00', '17:00:00', ''),
		(5, 'ó�������', '08:00:00', '17:00:00', ''),
		(6, '������', '08:00:00', '17:00:00', ''),
		(7, '��������', '08:00:00', '17:00:00', ''),
		(8, '����������', '08:00:00', '17:00:00', 'ĳ��������� �� �������� ����������� ����. �� ���������� ��� ������, �����, ����������, ������� ������ ����. ����������� ��������� ��������� ��������� ����� �� ������� �� ��.'),
		(9, '���������������', '08:00:00', '17:00:00', '������� ���������� �� �������� ����������� ��������-��������� ������, ����� ��: �������� ����������� ������, ���� �������������, ������ � ������ � ���������, ������-������ ������� �� ����.'),
		(10, '�����������', '08:00:00', '17:00:00', ''),
		(11, '������������', '08:00:00', '17:00:00', '˳���-������������ �������� ��� �������� ����� ����� � ���������� ������ �� �����. ��� ���� ��� ����������� ���� ������� ���, �������������, ��� � �� �������� �������� � ����� � ��� � �����, �� ������������ � ������ �� �������������.'),
		(12, '��������', '08:00:00', '17:00:00', ''),
		(13, '������������� (���)', '08:00:00', '17:00:00', '³�������������� ���������� ����, �������, ���; ��������� ���������� ����������� ��� �� ���������� ������������ ������ ��� �������. ׳���� �������� ��� ������������ ��������.'),
		(14, '������', '08:00:00', '17:00:00', '���������� ����, ������������ � ������ �������� �� ������� �� �������; ��� ������ ���������� ����������, ������������� �� ��������������� �� ������������� ��������.')
set identity_insert Doctor off
go
set identity_insert NameDay on
insert into NameDay
		(Id, NameD) 
	values 
		(1, '��������'),
		(2, '³������'),
		(3, '������'),
		(4, '������'),
		(5, '�"������'),
		(6, '�������')
set identity_insert NameDay off
go
set identity_insert WorkDay on
insert into WorkDay 
		(Id, DoctorId, DayId) 
	values 
		(1, 1, 6),
		(2, 2, 3),
		(3, 3, 6),
		(4, 4, 3),
		(5, 5, 6),
		(6, 6, 4),
		(7, 7, 6),
		(8, 8, 4),
		(9, 9, 2),
		(10, 10, 6),
		(11, 11, 6),
		(12, 12, 6),
		(13, 13, 6),
		(14, 14, 6)
set identity_insert WorkDay off
go
set identity_insert Patient on
insert into Patient 
		(Id, Surname, Name, Phone) 
	values 
		(1, '��������', '����', '(098) 355 56 89'),
		(2, '��������', '³����', '(067) 659 47 56'), 
		(3, '����������', '���������', '(050) 986 47 35'),
		(4, '�������', '�����', '(097) 454 15 15'),
		(5, '������', '����', '(066) 987 65 32')
set identity_insert Patient off
go
set identity_insert UserRecord on
insert into UserRecord 
		(Id, UserName, [Password], PatientId) 
	values 
		(1, 'Petrenko', '111', 1),
		(2, 'Ivanenko', '222', 2), 
		(3, 'Maksymenko', '333', 3),
		(4, 'Tarasiyk', '444', 4),
		(5, 'Tkachuk', '555', 5)
set identity_insert UserRecord off
go

-- foreign keys
alter table WorkDay add constraint
FK_WorkDay_DoctorId_Doctor_Id
foreign key (DoctorId)
references Doctor(Id);

alter table WorkDay add constraint
FK_WorkDay_DoctorId_NameDay_Id
foreign key (DayId)
references NameDay(Id);

alter table Appointment add constraint
FK_Appointment_DoctorId_Doctor_Id
foreign key (DoctorId)
references Doctor(Id);

alter table Appointment add constraint
FK_Appointment_PatientId_Patient_Id
foreign key (PatientId)
references Patient(Id);

alter table UserRecord add constraint
FK_UserRecord_PatientId_Patient_Id
foreign key (PatientId)
references Patient(Id);
